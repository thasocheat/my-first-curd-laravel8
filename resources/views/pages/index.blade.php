@extends('layouts.frontend')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12 mt-4 text-center">
                <div class="card card-body bg-light">
                    <h1>Welcome to the Laravel Application</h1>
                    <p>This is the Home Page</p>
                </div>
            </div>
        </div>
    </div>

@endsection